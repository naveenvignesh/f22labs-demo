var express = require("express");
var router = express.Router();

// Database helpers
var auth = require("../helpers/auth");

router.post("/login", (req, res) => {
  auth.getUser(req.body.email, (err, row) => {
    if (err) {
      res.json({
        message: "database-error",
        isAuth: false,
        error: err
      });
    } else {
      if (row.length) {
        const { password, email, id, first_name, last_name } = row[0];
        if (password === req.body.password) {
          res.status(200).json({
            message: "success",
            success: true,
            email,
            id,
            first_name,
            last_name
          });
        } else {
          res.status(201).json({
            message: "invalid credentials",
            success: false
          });
        }
      } else {
        res.status(202).json({
          message: "user email not found",
          success: false,
          error: err
        });
      }
    }
  });
});

router.post("/register", (req, res) => {
  auth.addUser(req.body.user, err => {
    if (err) {
      if (err.code === "ER_DUP_ENTRY") {
        res.status(400).json({
          message: "User already exists",
          success: false
        });
      } else {
        res.status(400).json({
          message: "generic-db-error",
          success: false,
          error: err
        });
      }
    } else {
      res.status(200).json({
        message: "user-created",
        user: req.body.user,
        success: true
      });
    }
  });
});

module.exports = router;
