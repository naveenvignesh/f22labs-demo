var express = require("express");
var router = express.Router();

// Database helpers
var posts = require("../helpers/posts");

router.put("/", (req, res) => {
  posts.addPost(req.body, error => {
    if (error) {
      res.status(202).json({
        error,
        success: false
      });
    } else {
      res.status(200).json({
        message: "post-added",
        success: true
      });
    }
  });
});

router.post("/", (req, res) => {
  posts.editPost(req.body, (error, data) => {
    if (error) {
      res.status(202).json({
        error,
        success: false
      });
    } else {
      if (!data.changedRows) {
        res.status(200).json({
          message: "no-post-updated",
          success: true
        });
      } else {
        res.status(200).json({
          message: "post-updated",
          success: true,
          data
        });
      }
    }
  });
});

router.get("/all", (req, res) => {
  posts.listPosts((err, data) => {
    if (err) {
      res.status(202).json({
        error: err,
        success: false
      });
    } else {
      res.status(200).json({
        success: true,
        data
      });
    }
  });
});

router.get("/:id", (req, res) => {
  posts.getPost(req.params.id, (err, data) => {
    if (err) {
      res.status(202).json({
        error: err,
        success: false
      });
    } else {
      if (!data.length) {
        res.status(202).json({
          success: false,
          message: "no-data"
        });
      } else {
        res.status(200).json({
          post: data[0],
          success: true
        });
      }
    }
  });
});

module.exports = router;
