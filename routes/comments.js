var express = require("express");
var router = express.Router();

// Database helpers
var comments = require("../helpers/comments");

router.put("/", (req, res) => {
  comments.addComment(req.body, (err, row) => {
    if (err) {
      res.status(202).json({
        message: "database-error",
        error: err
      });
    } else {
      res.status(200).json({
        message: "comment-added",
        success: true,
        data: row
      });
    }
  });
});

router.get("/bypost/:postId", (req, res) => {
  comments.listCommentsByPost(req.params.postId, (err, data) => {
    if (err) {
      res.status(202).json({
        error: err,
        success: false
      });
    } else {
      res.status(200).json({
        success: true,
        data
      });
    }
  });
});

module.exports = router;
