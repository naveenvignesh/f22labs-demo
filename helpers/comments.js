var db = require("./database");

const comments = {
  addComment: (data, cb) =>
    db.query(
      "insert into comments (id, comment, referenceId, userId, created, modified) values (?, ?, ?, ?, ?, ?)",
      [
        data.id,
        data.comment,
        data.referenceId,
        data.userId,
        data.created,
        data.modified
      ],
      cb
    ),
  listCommentsByPost: (postId, cb) =>
    db.query("select *, (select concat(first_name, ' ', last_name) as name from users where id = c.userId) as username from comments c where c.referenceId = ? order by modified desc", [postId], cb)
};

module.exports = comments;
