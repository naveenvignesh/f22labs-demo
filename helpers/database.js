var mysql = require("mysql");

var connection = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "mydb",
  port: 3306,
});

module.exports = connection;
