const db = require("./database");

const posts = {
  addPost: (data, cb) => {
    const {
      id = "",
      title = "",
      link = "",
      userId = 0,
      created = "",
      modified = "",
      type = "",
      post = ""
    } = data;
    db.query(
      "insert into posts (id, title, post, link, userId, created, modified, type) values (?, ?, ?, ?, ?, ?, ?, ?)",
      [id, title, post, link, userId, created, modified, type],
      cb
    );
  },
  editPost: (data, cb) => {
    const { id = "", title = "", link = "", modified = "", post = "" } = data;
    db.query(
      "update posts set title = ?, post = ?, link = ?, modified = ? where id = ?",
      [title, post, link, modified, id],
      cb
    );
  },
  listPosts: cb => db.query("select * from posts order by modified desc", cb),
  getPost: (postId, cb) =>
    db.query("select * from posts where id = ? limit 1", [postId], cb),
  removePost: (postId, cb) =>
    db.query("delete from posts where id = ?", [postId], cb)
};

module.exports = posts;
