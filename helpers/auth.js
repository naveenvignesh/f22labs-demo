const db = require("./database");

const auth = {
  getUser: (email, cb) =>
    db.query("select * from users where email = ?", [email], cb),
  addUser: (user, cb) =>
    db.query(
      "insert into users (first_name, last_name, email, password, created, modified) values (?, ?, ?, ?, ?, ?)",
      [
        user.firstName,
        user.lastName,
        user.email,
        user.password,
        user.created,
        user.modified
      ],
      cb
    ),
  removeUser: (email, cb) =>
    db.query("delete top(1) from users where email = ?", [email], cb)
};

module.exports = auth;
