import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import * as keys from "../constants/storageKeys";

// Pages
import Login from "../pages/login";
import Posts from "../pages/posts";
import Register from "../pages/register";
import PostDetails from "../pages/postDetails";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const user = JSON.parse(localStorage.getItem(keys.user));
      if (user && user.email) return <Component {...props} />;
      return <Redirect exact to="/" />;
    }}
  />
);

const routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/register" component={Register} />
      <PrivateRoute path="/posts" component={Posts} />
      <PrivateRoute path="/postDetails/:id" component={PostDetails} />
    </Switch>
  </Router>
);

export default routes;
