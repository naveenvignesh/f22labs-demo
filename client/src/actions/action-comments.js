import { PUT, GET } from "../libs/service";

import * as types from "../constants/actionTypes";

const requestAddComment = () => ({
  type: types.REQUEST_ADD_COMMENT
});

const requestAddCommentSuccess = result => ({
  type: types.REQUEST_ADD_COMMENT_SUCCESS,
  result
});

const requestAddCommentError = error => ({
  type: types.REQUEST_ADD_COMMENT_ERROR,
  error
});

const requestPostComments = () => ({
  type: types.REQUEST_POST_COMMENTS,
});

const requestPostCommentsSuccess = payload => ({
  type: types.REQUEST_POST_COMMENTS_SUCCESS,
  payload,
});

const requestPostCommentsError = error => ({
  type: types.REQUEST_POST_COMMENTS_ERROR,
  error,
});

export const getPostComments = postId => async dispatch => {
  dispatch(requestPostComments());
  try {
    const res = await GET(`/comments/bypost/${postId}`);
    dispatch(requestPostCommentsSuccess(res.data));
  } catch (err) {
    console.log(err);
    dispatch(requestPostCommentsError(err));
  }
};

export const addNewComment = comment => async dispatch => {
  dispatch(requestAddComment());
  try {
    const res = await PUT("/comments", comment);
    dispatch(requestAddCommentSuccess(res));
    if (res.success) {
      dispatch(getPostComments(comment.referenceId));
    }
  } catch (err) {
    dispatch(requestAddCommentError(err));
  }
};
