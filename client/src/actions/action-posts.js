import { GET, PUT, POST } from "../libs/service";

import * as types from "../constants/actionTypes";

const requestAllPosts = () => ({
  type: types.REQUEST_ALL_POSTS
});

const requestAllPostsSuccess = payload => ({
  type: types.REQUEST_ALL_POSTS_SUCCESS,
  payload
});

const requestAllPostsError = error => ({
  type: types.REQUEST_ALL_POSTS_ERROR,
  error
});

const requestPost = () => ({
  type: types.REQUEST_POST
});

const requestPostSuccess = payload => ({
  type: types.REQUEST_POST_SUCCESS,
  payload
});

const requestPostError = () => ({
  type: types.REQUEST_POST_ERROR
});

const requestAddPost = () => ({
  type: types.REQUEST_ADD_POST
});

const requestAddPostSuccess = result => ({
  type: types.REQUEST_ADD_POST_SUCCESS,
  result
});

const requestAddPostError = error => ({
  type: types.REQUEST_ADD_POST_ERROR,
  error
});

const requestUpdatePost = () => ({
  type: types.REQUEST_UPDATE_POST
});

const requestUpdatePostSuccess = result => ({
  type: types.REQUEST_UPDATE_POST_SUCCESS,
  result
});

const requestUpdatePostError = error => ({
  type: types.REQUEST_UPDATE_POST_ERROR,
  error
});

export const getAllPosts = () => async dispatch => {
  dispatch(requestAllPosts());
  try {
    const res = await GET("/posts/all");
    if (res.success) dispatch(requestAllPostsSuccess(res.data));
    else dispatch(requestAllPostsError(res));
  } catch (err) {
    dispatch(requestAllPostsError(err));
  }
};

export const addNewPost = post => async dispatch => {
  dispatch(requestAddPost());
  try {
    const res = await PUT("/posts", post);
    if (res.success) {
      dispatch(requestAddPostSuccess(res));
      dispatch(getAllPosts());
    }
  } catch (err) {
    dispatch(requestAddPostError(err));
  }
};

export const getPost = id => async dispatch => {
  dispatch(requestPost());
  try {
    const res = await GET(`/posts/${id}`);
    dispatch(requestPostSuccess(res.post));
  } catch (err) {
    dispatch(requestPostError(err));
  }
};

export const updatePost = post => async dispatch => {
  console.log("updatePost called");
  dispatch(requestUpdatePost());
  try {
    const res = await POST("/posts", post);
    console.log(res);
    if (res.success) {
      dispatch(requestUpdatePostSuccess(res));
      dispatch(getPost(post.id));
    } else {
      dispatch(requestUpdatePostError(res));
    }
  } catch (err) {
    console.log(err);
    dispatch(requestUpdatePostError(err));
  }
};
