import * as types from "../constants/actionTypes";
import * as keys from "../constants/storageKeys";

import { POST } from "../libs/service";

const requestLogin = () => ({
  type: types.REQUEST_LOGIN
});

const requestLoginSuccess = user => ({
  type: types.REQUEST_LOGIN_SUCCESS,
  payload: user
});

const requestLoginError = error => ({
  type: types.REQUEST_LOGIN_ERROR,
  error
});

const requestRegister = () => ({
  type: types.REQUEST_SIGNUP
});

const requestRegisterSuccess = res => ({
  type: types.REQUEST_SIGNUP_SUCCESS,
  payload: res
});

const requestRegisterFailure = error => ({
  type: types.REQUEST_SIGNUP_ERROR,
  error
});

const requestUser = () => ({
  type: types.REQUEST_USER
});

const requestUserSuccess = user => ({
  type: types.REQUEST_USER_SUCCESS,
  payload: user
});

const requestUserError = error => ({
  type: types.REQUEST_USER_ERROR,
  error
});

export const login = (email, password, nav) => async dispatch => {
  dispatch(requestLogin());
  try {
    const res = await POST("/auth/login", { email, password });
    dispatch(requestLoginSuccess(res));
    if (res && res.id) {
      localStorage.setItem(keys.user, JSON.stringify(res));
      nav.push("/posts");
    }

    if (!res.success) {
      dispatch(requestLoginError(res));
    }
  } catch (err) {
    dispatch(requestLoginError(err));
  }
};

export const register = (user, nav) => async dispatch => {
  dispatch(requestRegister());
  try {
    const res = await POST("/auth/register", { user });
    console.log(res);
    dispatch(requestRegisterSuccess(res));
    if (res.user) {
      nav.goBack();
    }
  } catch (err) {
    console.log(err);
    dispatch(requestRegisterFailure(err));
  }
};

export const getUser = () => dispatch => {
  dispatch(requestUser());
  try {
    const user = JSON.parse(localStorage.getItem(keys.user));
    dispatch(requestUserSuccess(user));
  } catch (err) {
    dispatch(requestUserError(err));
  }
};
