import React, { PureComponent } from "react";
import { Provider } from "react-redux";

import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import { library } from "@fortawesome/fontawesome-svg-core";
// import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faEdit,
  faSave,
  faThumbsUp,
  faThumbsDown
} from "@fortawesome/free-solid-svg-icons";

import AppRouter from "./config/routes";

import Store from "./config/store";

library.add(faEdit, faSave, faThumbsDown, faThumbsUp);

const options = {
  position: "bottom center",
  timeout: 3000,
  offset: "30px",
  transition: "scale"
};

class App extends PureComponent {
  render() {
    return (
      <Provider store={Store}>
        <AlertProvider template={AlertTemplate} {...options}>
          <AppRouter />
        </AlertProvider>
      </Provider>
    );
  }
}

export default App;
