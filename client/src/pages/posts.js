import React, { Component } from "react";
import PropTypes from "prop-types";

import { withRouter } from "react-router-dom";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import uuidV4 from "uuid/v4";

// Components
import Navbar from "../components/navbar";
import Input from "../components/input";
import Button from "../components/button";
import PostList from "../components/listviews/postlist";
import Loader from "../components/loader";

// Actions
import { getAllPosts, addNewPost } from "../actions/action-posts";

// Libs
import { logout, withAuth } from "../libs/auth";

// CSS
import "../styles/posts.css";

const styles = {
  postBtn: {
    marginTop: "10px",
    marginBottom: "10px",
    borderColor: "#fff",
    backgroundColor: "#333",
    color: "#fff"
  },
  articleTitleInput: {
    width: {
      value: "50%",
      important: true
    },
    marginBottom: 0
  }
};

class Posts extends Component {
  static getDerivedStateFromProps = nextProps => {
    console.log(nextProps.posts);
    return null;
  };

  state = {
    postData: {},
    postType: "post"
  };

  componentDidMount = () => {
    this.props.getAllPosts();
  };

  handleMenuItemPress = (item, index) => {
    if (index === 0) logout(this.props.history);
  };

  handlePostListItemPress = item => {
    this.props.history.push(`/postDetails/${item.id}`);
  };

  handleTextChange = (name, e) => {
    const text = e.target.value;
    this.setState(prevState => {
      let { postData } = prevState;
      postData[name] = text;
      return { postData };
    });
  };

  handlePostSubmit = type => {
    const { title, post, link } = this.state.postData;
    const { user, addNewPost } = this.props;
    const timeStamp = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");

    // if (post) post = `<p>${post.replace(/\n/g, "</p>\n<p>")}</p>`;
    let newPost = {
      id: uuidV4(),
      title,
      type,
      userId: user.id,
      created: timeStamp,
      modified: timeStamp
    };

    if (type === "post") {
      newPost = {
        ...newPost,
        post,
        link: ""
      };
    } else if (type === "link") {
      newPost = {
        ...newPost,
        link,
        post: ""
      };
    }
    if (title && (post || link)) {
      addNewPost(newPost);
      this.setState({ postData: {} });
    }
  };

  handleArticleSubmit = () => {};

  handlePostInputToggle = type => {
    this.setState({ postType: type });
    this.collapse.classList.toggle("show");
  };

  renderPostInput = () => (
    <div className="post-input">
      <Input
        style={styles.articleInput}
        type="text"
        value={this.state.postData.title || ""}
        placeholder="Post Title"
        onChange={e => this.handleTextChange("title", e)}
      />
      <textarea
        value={this.state.postData.post || ""}
        className="textbox form-control"
        placeholder="Enter a post"
        onChange={e => this.handleTextChange("post", e)}
      />
      <Button
        buttonText="Post"
        style={styles.postBtn}
        onClick={() => this.handlePostSubmit("post")}
      />
    </div>
  );

  renderArticleInput = () => (
    <div className="article-input">
      <Input
        style={styles.articleTitleInput}
        type="text"
        value={this.state.postData.title || ""}
        placeholder="Article Title"
        onChange={e => this.handleTextChange("title", e)}
      />
      <Input
        type="text"
        value={this.state.postData.link || ""}
        placeholder="Article Link"
        onChange={e => this.handleTextChange("link", e)}
      />
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => this.handlePostSubmit("link")}
      >
        <span className="glyphicon glyphicon-send">Add</span>
      </button>
    </div>
  );

  render() {
    const { posts, isFetching } = this.props;
    const { postType } = this.state;
    return (
      <div>
        <Navbar
          brand="F22 Labs Demo"
          menuItems={["Logout"]}
          onMenuItemPress={this.handleMenuItemPress}
        />
        <div className="container-fluid posts">
          <div className="main-canvas">
            <div className="add-btn-grp">
              <button
                className="btn btn-secondary"
                onClick={() => this.handlePostInputToggle("post")}
              >
                Write Post
              </button>
              <div className="text">OR</div>
              <button
                className="btn btn-secondary"
                onClick={() => this.handlePostInputToggle("link")}
              >
                Share Article
              </button>
            </div>
            <div className="collapse" ref={elem => (this.collapse = elem)}>
              {postType === "post"
                ? this.renderPostInput()
                : this.renderArticleInput()}
            </div>
            {isFetching ? (
              <Loader loaderText="Loading Posts..." />
            ) : (
              <PostList
                onItemPress={this.handlePostListItemPress}
                data={posts}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

Posts.propTypes = {
  history: PropTypes.object.isRequired,
  getAllPosts: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  posts: state.posts.data,
  result: state.posts.result,
  mutationError: state.posts.error,
  isFetching: state.loader.isFetching,
  isError: state.loader.isError,
  error: state.loader.error
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getAllPosts, addNewPost }, dispatch);

export default compose(
  withAuth(),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(withRouter(Posts));
