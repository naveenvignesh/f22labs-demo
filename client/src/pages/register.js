import React, { Component } from "react";
import PropTypes from "prop-types";

import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { withAlert } from "react-alert";

// Actions
import { register } from "../actions/action-auth";

// Components
import Input from "../components/input";
import Button from "../components/button";

// Libs
import { validateEmail, validatePassword } from '../libs/validate';

// CSS
import "../styles/register.css";

const styles = {
  btnReg: {
    borderColor: "#333",
    backgroundColor: "#000",
    color: "#fff",
    marginTop: "10px"
  }
};

class Register extends Component {
  state = {
    user: {}
  };

  handleTextChange = (name, e) => {
    const text = e.target.value;
    this.setState(prevState => {
      let { user } = prevState;
      user[name] = text;
      return { user };
    });
  };

  handleRegister = () => {
    const {
      firstName,
      lastName,
      email,
      password,
      copyPassword
    } = this.state.user;
    if (firstName && lastName && email && password && copyPassword) {
      if (!validateEmail(email)) {
        this.props.alert.error('Enter a valid email !!!');
        return;
      }

      if (!validatePassword(password)) {
        this.props.alert.error('A valid password must contain 6 or more characters');
        return;
      }

      if (password === copyPassword) {
        const timeStamp = new Date()
          .toISOString()
          .slice(0, 19)
          .replace("T", " ");

        const body = {
          ...this.state.user,
          created: timeStamp,
          modified: timeStamp
        };
        this.props.register(body, this.props.history);
      } else this.props.alert.error('Passwords don\'t match !!!');
    } else this.props.alert.info('Enter all fields !!!');
  };

  render() {
    return (
      <div className="container-fluid register">
        <div className="title">Register Yourself</div>
        <form>
          <Input
            type="text"
            placeholder="First Name"
            onChange={e => this.handleTextChange("firstName", e)}
          />
          <Input
            type="text"
            placeholder="Last Name"
            onChange={e => this.handleTextChange("lastName", e)}
          />
          <Input
            type="email"
            placeholder="E-Mail"
            onChange={e => this.handleTextChange("email", e)}
          />
          <Input
            type="password"
            placeholder="Password"
            onChange={e => this.handleTextChange("password", e)}
          />
          <Input
            type="password"
            placeholder="Re-enter Password"
            onChange={e => this.handleTextChange("copyPassword", e)}
          />
        </form>
        <Button
          buttonText="Register"
          style={styles.btnReg}
          onClick={this.handleRegister}
        />
      </div>
    );
  }
}

Register.propTypes = {
  history: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  isFetching: state.loader.isFetching,
  isError: state.loader.isError,
  error: state.loader.error
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ register }, dispatch);

export default compose(
  withRouter,
  withAlert,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Register);
