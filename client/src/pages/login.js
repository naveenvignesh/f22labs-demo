import React, { Component } from "react";
import PropTypes from "prop-types";

import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { withAlert } from "react-alert";

// Actions
import { login } from "../actions/action-auth";

// Components
import Input from "../components/input";
import Button from "../components/button";
import Loader from "../components/loader";

// CSS
import "../styles/login.css";

const styles = {
  registerBtn: {
    borderColor: "#333",
    backgroundColor: "#000",
    color: "#fff"
  }
};

class Login extends Component {
  static getDerivedStateFromProps = (nextProps, state) => {
    if (nextProps.error !== state.error) {
      return {
        error: nextProps.error,
      }
    }
    return null;
  }
  state = {
    email: "",
    password: "",
    error: {},
  };

  componentDidUpdate = (prevProps) => {
    const { user } = this.props;
    if (user && user.id) {
      this.props.history.push("/posts");
    }
  };

  handleTextChange = (name, e) => {
    this.setState({ [name]: e.target.value });
  };

  handleLogin = () => {
    const { email, password } = this.state;
    if (email && password) {
      this.props.login(email, password, this.props.history);
    } else {
      this.props.alert.error('Enter all fields !!!');
    }
  };

  render() {
    const { isFetching } = this.props;
    return (
      <div className="container-fluid login">
        <div className="title">F22 Labs Demo</div>
        {isFetching ? (
          <Loader loaderText="Loggin in..." />
        ) : (
          <div className="login">
            <form>
              <Input
                type="text"
                placeholder="E-Mail"
                onChange={e => this.handleTextChange("email", e)}
              />
              <Input
                type="password"
                placeholder="Password"
                onChange={e => this.handleTextChange("password", e)}
              />
            </form>
            <Button
              type="Button"
              buttonText="Login"
              onClick={this.handleLogin}
            />
            <Button
              type="Button"
              buttonText="Register"
              style={styles.registerBtn}
              onClick={() => this.props.history.push("/register")}
            />
          </div>
        )}
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  error: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.auth.user,
  isFetching: state.loader.isFetching,
  isError: state.loader.isError,
  error: state.loader.error
});

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);

export default compose(
  withRouter,
  withAlert,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Login);
