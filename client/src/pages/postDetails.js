import React, { Component } from "react";
import PropTypes from "prop-types";

import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { withAlert } from "react-alert";
import uuidV4 from "uuid/v4";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Libs
import { withAuth } from "../libs/auth";

// Actions
import { getPost, updatePost } from "../actions/action-posts";
import { addNewComment, getPostComments } from "../actions/action-comments";

// Components
import Navbar from "../components/navbar";
import CommentList from "../components/listviews/commentlist";
import Loader from "../components/loader";

// CSS
import "../styles/postDetails.css";

class PostDetails extends Component {
  state = {
    currentComment: "",
    updatedPost: "",
    postEditable: false
  };

  componentDidMount() {
    const { params } = this.props.match;
    this.props.getPost(params.id);
    this.props.getPostComments(params.id);
  }

  handleTextChange = (name, e) => {
    this.setState({ [name]: e.target.value });
  };

  handleMenuItemPress = (item, index) => {
    if (index === 0) this.props.history.goBack();
  };

  handleAddComment = () => {
    const { currentComment } = this.state;
    const { match, user, addNewComment } = this.props;

    const timeStamp = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");

    if (currentComment) {
      const newComment = {
        id: uuidV4(),
        comment: currentComment.trim(),
        referenceId: match.params.id,
        userId: user.id,
        created: timeStamp,
        modified: timeStamp
      };

      addNewComment(newComment);
    }
  };

  toggleEdit = () => {
    this.setState(prevState => ({ postEditable: !prevState.postEditable }));
  };

  formatPost = post => `<p>${post.replace(/\n/g, "</p>\n<p>")}</p>`;

  handlePostEdit = () => {
    const { updatedPost } = this.state;
    const { postData } = this.props;
    const timeStamp = new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ");
    if (postData.post !== updatedPost) {
      const updatedPostEntry = {
        ...postData,
        post: updatedPost,
        modified: timeStamp
      };
      this.props.updatePost(updatedPostEntry);
    }
  };

  render() {
    const { postData, commentData, isFetching, user } = this.props;
    const { postEditable } = this.state;
    return (
      <div>
        <Navbar
          brand={postData.title || ""}
          menuItems={["Go Back"]}
          onMenuItemPress={this.handleMenuItemPress}
        />
        <div className="container-fluid postDetails">
          <div className="content-container">
            {postEditable ? (
              <textarea
                defaultValue={postData.post}
                className="form-control post-edit-box"
                onChange={e => this.handleTextChange("updatedPost", e)}
              />
            ) : (
              <div
                className="content"
                dangerouslySetInnerHTML={{
                  __html: this.formatPost(postData.post || "")
                }}
              />
            )}
            {postData.userId === user.id && (
              <div className="btn-container">
                <button
                  type="button"
                  className="btn btn-edit"
                  onClick={this.toggleEdit}
                >
                  <FontAwesomeIcon className="icon" icon="edit" />
                </button>
                {postEditable && (
                  <button
                    type="button"
                    className="btn btn-save"
                    onClick={this.handlePostEdit}
                  >
                    <FontAwesomeIcon className="icon" icon="save" />
                  </button>
                )}
              </div>
            )}
          </div>
          <div className="comment-container">
            <textarea
              className="comment-box form-control"
              onChange={e => this.handleTextChange("currentComment", e)}
              placeholder="Your comment"
            />
            <button
              className="btn btn-secondary"
              onClick={this.handleAddComment}
            >
              Comment
            </button>
          </div>
          <div>
            <div className="comment-title">Comments</div>
            {isFetching ? (
              <Loader loaderText="Loading Posts..." />
            ) : (
              <CommentList data={commentData} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

PostDetails.propTypes = {
  history: PropTypes.object.isRequired,
  getPost: PropTypes.func.isRequired,
  addNewComment: PropTypes.func.isRequired,
  getPostComments: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  alert: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  postData: state.posts.data,
  commentData: state.comments.data,
  isFetching: state.loader.isFetching
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { getPost, addNewComment, getPostComments, updatePost },
    dispatch
  );

export default compose(
  withAuth(),
  withAlert,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(withRouter(PostDetails));
