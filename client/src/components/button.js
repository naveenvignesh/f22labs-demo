import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "../styles/component.css";

class Button extends PureComponent {
  state = {};
  render() {
    const { buttonText, ...buttonProps } = this.props;
    return (
      <button className="btn button" {...buttonProps}>
        {buttonText}
      </button>
    );
  }
}

Button.propTypes = {
  buttonText: PropTypes.string.isRequired
};

export default Button;
