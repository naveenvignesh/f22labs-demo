import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class Alert extends PureComponent {
  state = {};
  render() {
    const { type, message } = this.props;
    return <div className={`alert alert-${type}`}>{message}</div>;
  }
}

Alert.propTypes = {
  message: PropTypes.string.isRequired,
  type: PropTypes.oneOf([
    "success",
    "info",
    "warning",
    "danger",
    "primary",
    "secondary",
    "light",
    "dark"
  ])
};

Alert.propTypes = {
  type: "dark"
};
export default Alert;
