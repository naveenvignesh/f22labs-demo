import React, { PureComponent } from "react";
// import PropTypes from 'prop-types'

import "../styles/component.css";

class Input extends PureComponent {
  state = {};
  render() {
    const { ...inputProps } = this.props;
    return <input className="form-control input" {...inputProps} />;
  }
}

export default Input;
