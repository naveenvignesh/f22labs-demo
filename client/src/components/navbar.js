import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class Navbar extends PureComponent {
  state = {};
  render() {
    const { brand, menuItems, onMenuItemPress } = this.props;
    return (
      <nav className="navbar navbar-expand-sm navbar-custom">
        <div className="navbar-brand">{brand}</div>
        <ul className="navbar-nav">
          {menuItems.map((item, i) => (
            <li className="nav-item" key={i.toString()}>
              <button onClick={() => onMenuItemPress(item, i)} className="btn">
                {item}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    );
  }
}

Navbar.propTypes = {
  brand: PropTypes.any.isRequired,
  menuItems: PropTypes.arrayOf(PropTypes.string),
  onMenuItemPress: PropTypes.func
};

Navbar.defaultProps = {
  menuItems: [],
  onMenuItemPress: () => {}
};

export default Navbar;
