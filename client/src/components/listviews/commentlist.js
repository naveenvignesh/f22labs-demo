import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class CommentList extends PureComponent {
  render() {
    const { data = [] } = this.props;
    return (
      <ul className="list-group commentlist">
        {data.length > 0
          ? data.map((item, i) => (
              <li className="list-group-item" key={i.toString()}>
                <div className="comment-item-container">
                  <div className="comment">{item.comment}</div>
                  <div className="username">{item.username}</div>
                </div>
              </li>
            ))
          : null}
      </ul>
    );
  }
}

CommentList.propTypes = {
  data: PropTypes.array.isRequired
};

export default CommentList;
