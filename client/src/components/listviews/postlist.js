import React, { PureComponent } from "react";
import PropTypes from "prop-types";

// CSS
import "../../styles/component.css";

class PostList extends PureComponent {
  state = {};

  formatTime = timeStamp => new Date(timeStamp).toDateString();

  renderItem = (item, index, onItemPress) => {
    if (item.type === "link") {
      return (
        <li className="list-group-item" key={index.toString()}>
          <div className="post-item-container">
            <a href={item.link}>{item.title || "Link"}</a>
            <div className="time">{this.formatTime(item.created)}</div>
          </div>
        </li>
      );
    } else {
      return (
        <li className="list-group-item" key={index.toString()}>
          <div className="post-item-container">
            <div className="title">{item.title}</div>
            <button
              type="button"
              onClick={() => onItemPress(item)}
              className="btn btn-info"
            >
              View
            </button>
            <div className="time">{this.formatTime(item.created)}</div>
          </div>
        </li>
      );
    }
  };

  render() {
    const { data = [], onItemPress } = this.props;
    return (
      <ul className="list-group postlist">
        {data && data.length > 0
          ? data.map((item, i) => this.renderItem(item, i, onItemPress))
          : null}
      </ul>
    );
  }
}

PostList.defaultProps = {
  data: PropTypes.array.isRequired,
  onItemPress: PropTypes.func
};

PostList.propTypes = {
  onItemPress: () => {}
};

export default PostList;
