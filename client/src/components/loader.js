import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "../styles/component.css";

class Loader extends PureComponent {
  state = {};

  render() {
    const { loaderText } = this.props;
    return (
      <div className="loader-container">
        <div className="loader" />
        <div className="text">{loaderText}</div>
      </div>
    );
  }
}

Loader.propTypes = {
  loaderText: PropTypes.string.isRequired
};

export default Loader;
