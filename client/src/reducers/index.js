import { combineReducers } from "redux";

import auth from "./auth";
import loader from "./loader";
import posts from "./posts";
import comments from "./comments";

const rootReducer = combineReducers({
  auth,
  loader,
  posts,
  comments
});

export default rootReducer;
