import * as types from "../constants/actionTypes";

const initialState = {
  user: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_LOGIN_SUCCESS:
    case types.REQUEST_USER_SUCCESS:
      return Object.assign({}, state, { user: action.payload });
    default:
      return state;
  }
};
