import * as types from "../constants/actionTypes";

const initialState = {
  isFetching: false,
  isError: false,
  error: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_LOGIN:
    case types.REQUEST_SIGNUP:
    case types.REQUEST_ALL_POSTS:
    case types.REQUEST_POST_COMMENTS:
      return Object.assign({}, state, { isFetching: true });
    case types.REQUEST_LOGIN_SUCCESS:
    case types.REQUEST_SIGNUP_SUCCESS:
    case types.REQUEST_ALL_POSTS_SUCCESS:
    case types.REQUEST_POST_COMMENTS_SUCCESS:
      return Object.assign({}, state, { isFetching: false });
    case types.REQUEST_LOGIN_ERROR:
    case types.REQUEST_SIGNUP_ERROR:
    case types.REQUEST_ALL_POSTS_ERROR:
    case types.REQUEST_POST_COMMENTS_ERROR:
      return Object.assign({}, state, {
        isError: false,
        isFetching: false,
        error: action.error
      });
    default:
      return state;
  }
};
