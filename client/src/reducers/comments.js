import * as types from "../constants/actionTypes";

const initialState = {
  data: [],
  result: {},
  error: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_POST_COMMENTS_SUCCESS:
    case types.REQUEST_ADD_COMMENT_SUCCESS:
      return Object.assign({}, state, { data: action.payload, result: action.result });
    case types.REQUEST_ADD_COMMENT_ERROR:
      return Object.assign({}, state, { error: action.error });
    default:
      return state;
  }
};
