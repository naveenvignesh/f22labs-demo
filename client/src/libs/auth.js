import React, { Component } from "react";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { withRouter } from "react-router-dom";

import * as keys from "../constants/storageKeys";

// Actions
import { getUser } from "../actions/action-auth";

export const withAuth = () => WrappedComponent => {
  class Authentication extends Component {
    componentDidMount() {
      this.props.getUser();
    }

    componentDidUpdate() {
      const { user } = this.props;
      if (!(user && user.id)) {
        this.props.history.goBack();
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  Authentication.propTypes = {
    getUser: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired
  };

  const mapStateToProps = state => ({
    user: state.auth.user
  });

  const mapDispatchToProps = dispatch =>
    bindActionCreators({ getUser }, dispatch);

  const connectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Authentication);

  return withRouter(connectedComponent);
};

export const logout = nav => {
  localStorage.setItem(keys.user, JSON.stringify({}));
  nav.goBack();
};
