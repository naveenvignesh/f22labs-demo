import axios from "axios";

// API
import { API_END } from "../constants/api";

export const POST = (url, body, headers) =>
  new Promise(async (resolve, reject) => {
    try {
      const res = await axios.post(url, body, { headers, baseURL: API_END });
      resolve(res.data);
    } catch (err) {
      reject(err.data);
    }
  });

export const GET = (url, headers) =>
  new Promise(async (resolve, reject) => {
    try {
      const res = await axios.get(url, { headers, baseURL: API_END });
      resolve(res.data);
    } catch (err) {
      reject(err.data);
    }
  });

export const PUT = (url, body, headers) =>
  new Promise(async (resolve, reject) => {
    try {
      const res = await axios.put(url, body, { headers, baseURL: API_END });
      resolve(res.data);
    } catch (err) {
      reject(err.data);
    }
  });
